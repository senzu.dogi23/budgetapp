
<?php
/**
 * home controller
 */
class Home extends Controller 
{
    function index()
    {
       $errors = array();

       /* if (isset($_POST['login'])) {
        show($_POST);die();
       } */
       
       if(count($_POST) > 0)
       {
            //show($_POST);die();
            $user = new User();

            if($data = $user->fetchSingle('users', 'username', $_POST['username']))
            {
                //show($data);die();

                //$array = json_decode(json_encode($data), true);
                
                $row = $user->fetchJoin('users', 'userdetails', 'userid', $data[0]->userid);
                $row = $row[0];
                
                if($_POST['password'] == $data[0]->password)
               {    
                    Auth::authenticate($row);
                    //show($_SESSION['USER']);die();
                    $_SESSION['login'] = 1;                                       

                    $_SESSION['last_login_timestamp'] = time();

                    //show($row);die();
                    return $this->redirect('dashboard');
               } 

            }
            //Add errors if there are any
            $errors['email'] = "Wrong Username or Password";
        }
        
       $this->view('login',[
        'errors' => $errors,
        ]);
    } 
}