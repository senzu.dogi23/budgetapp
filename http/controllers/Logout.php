
<?php
/**
 * home controller
 */
class Logout extends Controller 
{
    function index()
    {
        if(!Auth::logged_in())
        {
            $this->redirect('/');
        }

        session_destroy();
       
       $this->redirect('/',[

        ]);
    } 
}