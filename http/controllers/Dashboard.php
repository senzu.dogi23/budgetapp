
<?php
/**
 * home controller
 */
class Dashboard extends Controller 
{
    function index()
    {
        if(!Auth::logged_in())
        {
            $this->redirect('/');
        }

        $errors = array();

       
       if(count($_POST) > 0)
       {
            
            $user = new User();
            if ($user->validate($_POST)) {
                $_POST['userid'] = Auth::get_id();
                $_POST['month'] = date('m');
                $_POST['year'] = date('Y');
//show($_POST);die();
                $user->insert_into('budgets', $_POST);
                
            }

            //Add errors if there are any
            $errors['email'] = "Wrong Username or Password";
        }

        $user = new User();
        $userid = Auth::get_id();
        $date = date('m');

        $row = array();
        
        if(isset($_GET['find'])){
            $find = trim($_GET['find']);

            if ($find == 2) {
                return $this->redirect('dashboard');
            }else{
                $query = "SELECT * FROM budgets WHERE userid = '$userid'
                AND `month`='$date' AND budgettype=:find ORDER BY budgetdate DESC";
                //echo $query;die();
                $arr['find'] = $find;

                $row =$user->query($query, $arr);
            }
        }else{
            $row = $user->query("SELECT * FROM budgets WHERE userid = '$userid'
                            AND `month`='$date' ORDER BY budgetdate DESC");

        }
        
        

        $income = 0.00;

        $calc = new Calc();

        $income = $calc->calculateBudget('income', $userid);
        

        $expenditure = 0.00;

        $expenditure = $calc->calculateBudget('expenditure', $userid);

        $balance = $income - $expenditure;
    
        //echo $income;die();
        
        $budget['income'] = $income;
        $budget['expenditure'] = $expenditure;
        $budget['balance'] = $income - $expenditure;
       
       $this->view('dashboard',[
            'rows'      =>  $row,
            "errors"    =>  $errors,
            'budget'    =>  $budget,
        ]);
    }


    function detailed(){
        
        $user = new User();
        $calc = new Calc();

        $userid = Auth::get_id();
        $date = date('Y');
        //echo $date;

        $income = 0.00;
        $expenditure = 0.00;

        $monthIncome = array();
        $monthExpenses = array();

        $years = array();

        $calc = new Calc();

        $row = array();
        
        $month = array(1=>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May', 
                        6=>'June', 7=>'July', 8=>'August', 9=>'September', 10=>'October', 
                                    11=>'November', 12=>'December');
        //show($month);
        
        if(isset($_GET['year'])){
            $find = trim($_GET['year']);

            if ($find == 0 && $_GET['year']==$date) {
                return $this->redirect('dashboard/detailed');
            }else{
                
                foreach ($month as $key => $mon) {
                    $monthIncome[$mon] = $calc->calculateYearly('income', (int)$key, $find);
                    $monthExpenses[$mon] = $calc->calculateYearly('expendiure', (int)$key, $find);                    
                }

                $years = $user->query('SELECT DISTINCT `year` FROM budgets ORDER BY `year` DESC');
            }
        }else{
            foreach ($month as $key => $mon) {
                
                $monthIncome[$mon] = $calc->calculateYearly('income', (int)$key, $date);
                $monthExpenses[$mon] = $calc->calculateYearly('expendiure', (int)$key, $date);
                
            }//show($monthExpenses);

            $years = $user->query('SELECT DISTINCT `year` FROM budgets ORDER BY `year` DESC');

        }   //show($years);
    
        //echo $income;die();
        

        return $this->view('monthly', [
            'rows'      =>  $row,
            'expenses'    => $monthExpenses,
            'income'    => $monthIncome,
            'years'     => $years,
        ]);
    }

    function editYearly($year = null){
        
        if(!Auth::logged_in())
        {
            $this->redirect('/');
        }

        $errors = array();

       
       if(count($_POST) > 0)
       {
            
            $user = new User();
            if ($user->validate($_POST)) {
                $_POST['userid'] = Auth::get_id();
                $_POST['month'] = date('m');
                $_POST['year'] = date('Y');
//show($_POST);die();
                $user->insert_into('budgets', $_POST);
                
            }

            //Add errors if there are any
            $errors['email'] = "Wrong Username or Password";
        }

        $user = new User();
        $userid = Auth::get_id();
        $date = date('m');

        $row = array();
        
        if(isset($_GET['find'])){
            $find = trim($_GET['find']);

            if ($find == 2) {
                return $this->redirect('dashboard');
            }else{
                $query = "SELECT * FROM budgets WHERE userid = '$userid'
                AND `month`='$date' AND budgettype=:find ORDER BY budgetdate DESC";
                //echo $query;die();
                $arr['find'] = $find;

                $row =$user->query($query, $arr);
            }
        }else{
            $row = $user->query("SELECT * FROM budgets WHERE userid = '$userid'
                            AND `month`='$date' ORDER BY budgetdate DESC");

        }
        
        

        $income = 0.00;

        $calc = new Calc();

        $income = $calc->calculateBudget('income', $userid);
        

        $expenditure = 0.00;

        $expenditure = $calc->calculateBudget('expenditure', $userid);

        $balance = $income - $expenditure;
    
        //echo $income;die();
        
        $budget['income'] = $income;
        $budget['expenditure'] = $expenditure;
        $budget['balance'] = $income - $expenditure;
       
       $this->view('dashboard',[
            'rows'      =>  $row,
            "errors"    =>  $errors,
            'budget'    =>  $budget,
        ]);
    }

    function delete($userid = null){
        $user = new User();

        if ($user->delete('budgets', $userid)) {
            echo "<script>alert('Record deleted')</script>";
        }

        return $this->redirect('dashboard');
    }
}