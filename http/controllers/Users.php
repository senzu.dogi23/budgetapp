

<?php
/**
 * User controller
 */
class Users extends Controller 
{
    function index()
    {
       $errors = array();

       /* if (isset($_POST['login'])) {
        show($_POST);die();
       } */
       
       if(count($_POST) > 0)
       {
            //show($_POST);die();
            $user = new User();

            if($data = $user->validate($_POST))
            {
                $_POST['userid'] = generateUserID();

                unset($_POST['register']);
                
                //show($_POST);die();
               $user->insert_into('userdetails',$_POST);
               $user->insert_into('users', [
                    'userid'=>$_POST['userid'], 
                    'username'=>$_POST['email'],
                    'password'=> $user->hash_password($_POST['phone'])
               ]);
            }else{
                $errors = $user->errors;
            }
            //Add errors if there are any
            
        }
        
       $this->view('user',[
            'errors' => $errors,
        ]);
    } 

    function resetsystem($id = null)
    {
        if(!Auth::logged_in())
        {
            $this->redirect('/');
        }

        if ($id == 1) {
            $user = new User();

            $user->query("TRUNCATE budgets");
            $user->query("TRUNCATE userdetails");
            $user->query("TRUNCATE users");

            $this->redirect('logout');
       
        }

       $this->view('resetsystem',[

        ]);
    }

    function resetpassword()
    {
        if(!Auth::logged_in())
        {
            $this->redirect('/');
        }
       
       $this->view('resetpassword',[

        ]);
    }

    function export()
    {
        if(!Auth::logged_in())
        {
            $this->redirect('/');
        }
       
       $this->view('export',[

        ]);
    }
}