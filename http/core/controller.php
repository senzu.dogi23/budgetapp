
<?php
/**
 * Main Controller class
 */
class Controller 
{
    public function view($view, $data = array())
    {
        $view = strtolower($view);

        extract($data);
        
        if(file_exists("../http/views/". $view .".view.php"))
        {
            require("../http/views/". $view .".view.php");
            
        }
        else
        {
            require("../http/views/404.view.php");
        }
    }

    public function load_madel($model)
    {
        //echo $model;die();
        if(file_exists("../http/models/".ucfirst($model).".php"))
        {
            require("../http/models/".ucfirst($model).".php");
            return $model = new $model();
        }
        return false;
    }
    
    public function redirect($link)
    {
        header("Location: ". ROOT . "/".trim($link, "/"));
        die;
    }
}