<?php

require("config.php");
require("app.php");
require("database.php");
require("model.php");
require("helpers.php");
require("controller.php");
require("calc.php");



spl_autoload_register(function($class_name)
{
   require("../http/models/" . ucfirst($class_name). ".php");
});


?>