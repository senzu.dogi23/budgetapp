
<?php
/**
 * Main Model
 */
class Model extends Database 
{
    //protected $table = "user" ;
    public $errors = array();

    public function __construct()
    {
       
    }

    public function where($column, $value, $order = 'desc')
    {
        echo $this->table;die();
        $column = addslashes($column);
        $query = "select * from $this->table where $column = :value order by id $order";
  
        $data = $this->query(
            $query,[
            'value' => $value
       ]);

       //run fuctions after select
        if(is_array($data)){
            if(property_exists($this, 'afterSelect'))
            {
                foreach($this->afterSelect as $func)
                {
                    $data = $this->$func($data);
                }
            }
        }
        return $data;
    }

   public function fetchFrom($tb_name, $order='ASC')
        {
            
            $query = "SELECT * FROM $tb_name ORDER BY id $order";
            $data = $this->query($query);

            //run functions after select
            if (is_array($data)) {
                if (property_exists($this, 'afterSelect')) {
                    foreach($this->afterSelect as $func){
                        $data = $this->$func($data);
                    }
                }
            }
            

            return $data;
        }

    public function fetchSingle($tb_name,$column, $value)
    {
        $column = addslashes($column);
        $query = "select * from $tb_name where $column = :value ORDER BY id ASC";
  
        $data = $this->query(
            $query,[
            'value' => $value
       ]);

       //run fuctions after select
        if(is_array($data)){
            if(property_exists($this, 'afterSelect'))
            {
                foreach($this->afterSelect as $func)
                {
                    $data = $this->$func($data);
                }
            }
        }
        return $data;
    }

     public function fetchJoin($tb_name1,$tb_name2, $column, $value)
    {
        $column = addslashes($column);
        $query = "select * from $tb_name1 JOIN $tb_name2 ON $tb_name1.$column = $tb_name2.$column where $tb_name1.$column = :value";
  
        $data = $this->query(
            $query,[
            'value' => $value
       ]);

       //run fuctions after select
        if(is_array($data)){
            if(property_exists($this, 'afterSelect'))
            {
                foreach($this->afterSelect as $func)
                {
                    $data = $this->$func($data);
                }
            }
        }
        return $data;
    }

    public function first($column, $value, $order = 'desc')
    {
        
        $column = addslashes($column);
        $query = "select * from $this->table where $column = :value order by id $order";
        $data = $this->query(
            $query,[
            'value' => $value
       ]);

        if(is_array($data)){
            if(property_exists($this, 'afterSelect'))
            {
                foreach($this->afterSelect as $func)
                {
                    $data = $this->$func($data);
                }
            }
        }
        if(is_array($data))
        {
            $data = $data[0];
        }
        return $data;
    }

    public function fetchAll($order = 'desc')
    {
        
        //$column = addslashes($column);
        $query = "select * from $this->table order by id $order";
       
        $data = $this->query($query);

        if(is_array($data)){
            if(property_exists($this, 'afterSelect'))
            {
                foreach($this->afterSelect as $func)
                {
                    $data = $this->$func($data);
                }
            }
        }
        if(is_array($data))
        {
            $data = $data[0];
        }
        return $data;
    }


    public function insert($data)
    {
        
        //this removes unwanted columns
        if(property_exists($this, 'allowedColumns'))
        {
            foreach($data as $key => $column)
            {
                if(!in_array($key, $this->allowedColumns))
                {
                    unset($data[$key]);
                }
            }
        }

        //this run functions before insert
        if(property_exists($this, 'beforeInsert'))
        {
            foreach($this->beforeInsert as $func)
            {
                $data = $this->$func($data);
            }
        }

        $keys = array_keys($data);
        $column = implode(',', $keys);
        $values = implode(',:', $keys);
        $query = "insert into $this->table ($column) values (:$values)";
        
        return $this->query($query, $data);
    }

    public function insert_into($table, $data)
    {
        $keys = array_keys($data);
        $column = implode(',', $keys);
        $values = implode(',:', $keys);
        $query = "insert into $table ($column) values (:$values)";
        
        return $this->query($query, $data);
    }

    public function update($table='',$col = '', $id, $data)
        {
            $data[$col] = $id;
            $str = '';

            foreach ($data as $key => $value) {
                $str .= $key. "=:". $key. ",";
            }

            $str = trim($str, ",");


            if ($table=='') {
                $query = "UPDATE $this->table SET $str WHERE member_id = :id";
            }else {
                $query = "UPDATE $table SET $str WHERE $col = '$id'";
            
            }

            //echo "$query";die();
            return $this->query($query,$data);
        }


        public function updateSpecific($table='', $id, $data)
        {
            $data['id'] = $id;
            $str = array();


            $str = $data;
            
                $query = "UPDATE $table SET $str WHERE member_id = '$id'";
            

            return $this->query($query,$data);
        }


    public function delete($table, $id)
    {

        $query = "delete from $table where id = '$id'";
        
        return $this->query($query);
    }

    public function findAll($order = 'desc')
    {
        
        $query = "select * from $this->table order by id $order";
        $data = $this->query($query);
        

         //this run functions after select
        if(is_array($data)){
            if(property_exists($this, 'afterSelect'))
            {
                foreach($this->afterSelect as $func)
                {
                    $data = $this->$func($data);
                }
            }
        }
         return $data;
    }



    
}