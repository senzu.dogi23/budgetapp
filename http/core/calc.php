<?php
    class Calc{
        

        public function __construct()
        {
            # code...
        }

        public function calculateBudget($budget = null, $userid = null)
        {
            $user = new User();
            $date = date('m');
            
            $income = 0.00;
            
            if ($budget == 'income') {
                $inc = $user->query("SELECT amount FROM budgets WHERE userid = '$userid' 
                                AND `month`='$date' AND budgettype='1'");

                if ($inc) {
                    foreach ($inc as $inco) {
                        $income +=(double)$inco->amount;
                    }
            
                }
            }else {
                $inc = $user->query("SELECT amount FROM budgets WHERE userid = '$userid' 
                            AND `month`='$date' AND budgettype='0'");

                if ($inc) {
                    foreach ($inc as $inco) {
                        $income +=(double)$inco->amount;
                    }
            
                }
            }
            return $income;
        }

        public function calculateYearly($budget = null, $month, $year = null)
        {
            $user = new User();
            $date = date('m');
            
            $income = 0.00;
            
            if ($budget == 'income') {
                $inc = $user->query("SELECT amount FROM budgets WHERE `year` = '$year' 
                                   AND budgettype='1' AND `month`='$month'");

                if ($inc) {
                    foreach ($inc as $inco) {
                        $income +=(double)$inco->amount;
                    }
            
                }
            }else {
                $inc = $user->query("SELECT amount FROM budgets WHERE `year` = '$year' 
                                    AND budgettype='0' AND `month`='$month'");

                if ($inc) {
                    foreach ($inc as $inco) {
                        $income +=(double)$inco->amount;
                    }
            
                }
            }
            return $income;
        }

        public function calculateMonthly($budget = null, $month = null, $year=null)
        {
            $user = new User();
            $date = date('m');

            $userid = Auth::get_id(); 
            
            $income = 0.00;
            
            if ($budget == 'income') {
                $inc = $user->query("SELECT amount FROM budgets WHERE userid = '$userid' 
                                   AND budgettype='1' AND `month`='$month' AND `year` = '$year'");

                if ($inc) {
                    foreach ($inc as $inco) {
                        $income +=(double)$inco->amount;
                    }
            
                }
            }else {
                $inc = $user->query("SELECT amount FROM budgets WHERE userid = '$userid' 
                                    AND budgettype='0' AND `month`='$month' AND `year`='$year'");

                if ($inc) {
                    foreach ($inc as $inco) {
                        $income +=(double)$inco->amount;
                    }
            
                }
            }
            return $income;
        }

        
    }