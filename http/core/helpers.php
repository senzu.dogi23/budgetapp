<?php
function get_var($key, $default ="")
{
    if(isset($_POST[$key]))
    {
        return $_POST[$key];
    }
    return $default;
}

function get_select($key, $value)
{
    if(isset($_POST[$key]))
    {
       if($_POST[$key] == $value)
       {
           return "Selected";
       }
    }
    return "";
}

function esc($var)
{
    return htmlspecialchars($var);
}

function random_string($length)
{
    $array = array(0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    $text = "";
    for($x = 0; $x < $length; $x++)
    {
        $random = rand(0,61);
        $text .= $array[$random];
    }
    return $text;
}

function get_date($date)
{
    return date("jS F, Y",strtotime($date));
}

function get_day(){
    $datetime = DateTime::createFromFormat('d/m/Y', '05/06/2021');
    
    return $datetime->format('l');
}

function get_date_num($date)
{
    return date("d-m-Y",strtotime($date));
}

function show($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";

    die();
}

function get_image($image, $gender='male')
{
    if(!file_exists($image))
    {
        $image = ASSETS. '/img/female_user.png';
        if($gender == 'male')
        {
            $image = ASSETS. '/img/male_user.png';
        }
    }else{


        //$img = new Image();
       //$image = ROOT . "/". $img->profile_thumb($image);
        $image = ROOT . "/".$image;
    }
    return $image;
}

function view_path($view)
{
    if(file_exists("http/views/". $view . ".inc.php"))
    {
        return("http/views/". $view . ".inc.php");
    }else{
        return("http/views/404.view.php");
    }
}

function generateUserID()
    {
        $member = new User();

        $church = 'usr';
        $data = [];
        $data = $member->fetchFrom('userdetails');

        //show($data);die();

        $num = 0;
        if (!empty($data)) {
            $num = count($data);
        }
        
        $leadingZeros = '';
        $lastmember = substr($data[$num - 1]->userid, 3);
        
        if ($num < 10) {
            $leadingZeros = '0000';
        }elseif ($num < 100) {
            $leadingZeros = '000';
        }elseif ($num < 1000) {
            $leadingZeros = '00';
        }elseif ($num < 10000) {
            $leadingZeros = '0';
        }

        $lastmember= $lastmember + 1;

        $data = $church.$leadingZeros.$lastmember;

        //echo $data;die();
                    
        return $data;
    }

    function calculateMonthly($budget = null, $month = null, $year=null)
    {
        $user = new User();
        $date = date('m');

        $userid = Auth::get_id(); 
        
        $income = 0.00;
        
        if ($budget == 'income') {
            $inc = $user->query("SELECT amount FROM budgets WHERE userid = '$userid' 
                               AND budgettype='1' AND `month`='$month' AND `year` = '$year'");

            if ($inc) {
                foreach ($inc as $inco) {
                    $income +=(double)$inco->amount;
                }
        
            }
        }else {
            $inc = $user->query("SELECT amount FROM budgets WHERE userid = '$userid' 
                                AND budgettype='0' AND `month`='$month' AND `year`='$year'");

            if ($inc) {
                foreach ($inc as $inco) {
                    $income +=(double)$inco->amount;
                }
        
            }
        }
        return $income;
    }