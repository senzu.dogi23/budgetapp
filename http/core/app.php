<?php
/**
 * main app file
 */

class App
{
    protected $controller = "/";
    protected $params = array();
    protected $method = "index";

    public function __construct()
    {
        $URL = $this->getURL();
        $URL[0] = ucfirst($URL[0]);
        
        //echo "<pre>"; print_r($URL);die();
        if(file_exists("../http/controllers/".$URL[0].".php"))
        {
            $this->controller = ucfirst($URL[0]);
            unset($URL[0]);

        }
        else{
        }

        $this->controller = ucwords($this->controller);
        //echo $this->controller;die();
        require "../http/controllers/".$this->controller.".php";
        $this->controller = new $this->controller();

        if(isset($URL[1]))
        {
            
            
            if(method_exists($this->controller, ucwords($URL[1])))
            {
                $this->method = ucwords($URL[1]);
                unset($URL[1]);

            }
        }
        $URL = array_values($URL);
       $this->params = $URL;
        call_user_func_array([$this->controller, $this->method],$this->params);
    }

    private function getURL()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : "home";
       return explode("/", filter_var(trim($url, "/")), FILTER_SANITIZE_URL);
    }
    
}