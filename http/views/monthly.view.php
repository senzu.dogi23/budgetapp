<style>
    input {
        border-width: 0; 
        box-shadow: none;
    }
input:focus {
    border-width: 0; 
    box-shadow: none;
    }
</style>

<?php $this->view('includes/header')?>
<?php $this->view('includes/sidebar')?>

            <h2 class="text-center">YEARLY BUDGET SUMMARY</h2>
                
            <div class="card-group ">
                    <form action="" method="GET" class="">
                        <div class="input-group float-end" style="max-width: 250px;margin-left:100px">
                            <div class="input-group-prepend " >
                                <button class="input-group-text btn-sm btn-primary" id="basic-addon1"><i class="bi bi-search btn-sm btn-primary"></i></button>
                            </div>
                            
                            <select id="year" value="<?=isset($_GET['year']) ? $_GET['year']:''?>" name="year" class="form-control form-control-sm"></span>
                                <?php foreach ($years as $year):?>
                                    <option <?=get_select('year', '$year->year')?> value="<?=$year->year?>"><?=$year->year?></option> 
                                <?php endforeach?>
                                                                                                    
                            </select>
                        </div>
                    </form>
                    <table class="table table-striped table-hover table-bordered">                        

                    <tr class="" ><th style="width:2px;">#</th><th style="width:100px;">Month</th><th style="width:100px;">Total Income</th><th style="width:150px;">Total Expenses</th><th style="width:100px;">Balance</th><th >Budget Status</th><th style="width:70px;">Action</th>
                    </tr>
                        <?php $count = 1; foreach ($income as $key => $value) : ?>
                            <?php if ($value != 0): ?>
                            <tr class="" >
                                <td><?=$count?></td><td><?=$key?></td><td ><?=$value?></td><td id="values"><?=$expenses[$key]?></td>
                                <td><?=(int)$income[$key] - (int)$expenses[$key]?></td>
                                <td></td>
                                <td>
                                    <a onclick="deletes(event)" href="" class="btn btn-sm btn-danger" style="backgroud-color:red" ><i class="bi bi-trash"></i></a>
<!--                                     <a href="<?=ROOT?>/dashboard/editYear/<?=$key?>" class="btn btn-sm btn-success" style="backgroud-color:red" ><i class="fa fa-edit"></i></a>
 -->
                                </td>
                            </tr>
                            <?php $count+=1?>
                            <?php endif?>
                        <?php endforeach ?>
                        

                    </table>
                    
                    <div class="row justif-content-right" style="margin-left: 150px"><a href="<?=ROOT?>/dashboard"><i>Back</i></a></div>
                    
                    <!-- <div class="container">
                        <canvas id="myChart"></canvas>
                    </div> -->
                                        
        <!-- /#page-content-wrapper -->

    <?php $this->view('includes/footer')?>


<script type="text/javascript">
    function deletes(e)
    {
        a=confirm('Are You Sure To Remove This Record ?')
        if(a)
        {
            window.location.href='<?=ROOT?>/dashboard/delete/'+id;
        }else{
            e.preventDefault();
        }
    }

    document.addEventListener('DOMContentLoaded', ()=>{
        const ctx = document.getElementById('myChart');

        let value = document.querySelector('#values').value;

        new Chart(ctx, {
            type: 'bar',
            data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June','July', 'August', 'September', 'October', 'November', 'December'],
            datasets: [{
                label: '# of Votes',
                data: [value, 19, 3, 5, 2, 3],
                borderWidth: 0.5
            }]
            },
            options: {
            scales: {
                y: {
                beginAtZero: true
                }
            }
            }
        });
            })
</script>
