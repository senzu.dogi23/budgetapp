<style>
    input {
        border-width: 0; 
        box-shadow: none;
    }
input:focus {
    border-width: 0; 
    box-shadow: none;
    }
</style>

<?php $this->view('includes/header')?>
<?php $this->view('includes/sidebar')?>

     <div class="bg-emerald-500 w-52 h-52"></div>
        </div>

            <h2 class="text-center">MONTHLY BUDGET</h2>
                
            <div class="card-group ">
            <div style=""><h3>Budget Dated: <?php//get_day()?>  <?=date('F, Y')?> </h3></div>
                    <form action="" method="GET" class="">
                        <div class="input-group float-end" style="max-width: 250px;margin-left:100px">
                            <div class="input-group-prepend " >
                                <button class="input-group-text btn-sm btn-primary" id="basic-addon1"><i class="bi bi-search btn-sm btn-primary"></i></button>
                            </div>
                            
                            <select id="month" value="<?=isset($_GET['find']) ? $_GET['find']:''?>" name="find" class="form-control form-control-sm"></span>
                                <option <?=get_select('month', 'All')?> value="0">All</option> 
                                <option <?=get_select('month', 'January')?> value="1">January</option>    
                                <option <?=get_select('month', 'February')?> value="2">February</option>
                                <option <?=get_select('month', 'March')?> value="3">March</option>
                                <option <?=get_select('month', 'April')?> value="4">April</option>
                                <option <?=get_select('month', 'May')?> value="5">May</option>
                                <option <?=get_select('month', 'June')?> value="6">June</option>
                                <option <?=get_select('month', 'July')?> value="7">July</option>
                                <option <?=get_select('month', 'August')?> value="8">August</option>
                                <option <?=get_select('month', 'September')?> value="9">September</option>
                                <option <?=get_select('month', 'October')?> value="10">October</option>
                                <option <?=get_select('month', 'November')?> value="11">November</option>
                                <option <?=get_select('month', 'December')?> value="12">December</option>
                                                                    
                            </select>
                            <select id="year" value="<?=isset($_GET['year']) ? $_GET['year']:''?>" name="year" class="form-control form-control-sm"></span>
                                <?php foreach ($years as $year):?>
                                    <option <?=get_select('year', '$year->year')?> value="<?=$year->year?>"><?=$year->year?></option> 
                                <?php endforeach?>
                                                                                                    
                            </select>
                        </div>
                    </form>
                    <table class="table table-striped table-hover table-bordered">                        

                    <tr class="" ><th style="width:2px;">#</th><th>Budget Type</th><th>Sub Type</th><th style="width:100px;">Amount</th><th>Budget Date</th><th>Description</th><th>Action</th>
                    </tr>

                    <?php if($rows):?>
                    <?php $count = 1; foreach ($rows as $row): ?>
                        <tr class="">
                            <td class="bg-white"><?=$count?></td>
                            <td class="bg-white">
                                <?php if ($row->budgettype == 1):?>
                                    <?php echo "Income"; ?>
                                <?php else: ?>
                                    <?php echo "Expenditure"; ?>
                                <?php endif ?>
                                
                            </td>
                            <td class="bg-white" ><?=$row->subtype?></td>
                            <td class="bg-white"><?=$row->amount?></td>
                            <td class="bg-white"><?=$row->budgetdate?></td>
                            <td class="bg-white"><?=$row->description?></td>
                            <td><a onclick="deletes(event)" href="<?=ROOT?>/dashboard/delete/<?=$row->id?>" class="btn btn-sm btn-danger" style="backgroud-color:red" ><i class="bi bi-trash"></i></a></td>
                            
                        </tr>                    
                        
                    <?php $count+=1; endforeach ?>
                    <?php endif?>
                    
                    </table>
                    <h5>Total Income(Ghc): <?=$budget['income'];?> </h5> <h5 style="margin-left: 100px">Total Expenditure(Ghc): <?=$budget['expenditure'];?> </h5> <h5 style="margin-left: 100px">Balance(Ghc): <?=$budget['balance'];?>  </h5>
                    
                    <div class="row justif-content-right" style="margin-left: 150px"><a href="<?=ROOT?>/dashboard"><i>Back</i></a></div>
                    
                     
        <!-- /#page-content-wrapper -->

    <?php $this->view('includes/footer')?><input type="text" class="form-control" name="datepicker" id="datepicker" />


<script type="text/javascript">
    function deletes(e)
    {
        a=confirm('Are You Sure To Remove This Record ?')
        if(a)
        {
            window.location.href='<?=ROOT?>/dashboard/delete/'+id;
        }else{
            e.preventDefault();
        }
    }
</script>
   