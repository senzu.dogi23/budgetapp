<?php $this->view('includes/header')?>

<section class="vh-100">
  <div class="container-fluid h-custom">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-9 col-lg-6 col-xl-6">
      <!--https://macaubusiness.s3.ap-southeast-1.amazonaws.com/2021/05/budget.jpeg-->
        <img src="<?=ASSETS?>/images/bud1.png"
          class="img-fluid" alt="Sample image" >
      </div>

      <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
        <form method="POST" class="row g-3 needs-validation" nonvalidate>
        
          <div class="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
            <p class="lead fw-normal mb-0 me-3">Sign in with</p>
            <button type="button" class="btn btn-primary btn-floating mx-1">
              <i class="bi bi-facebook"></i>
            </button>

            <button type="button" class="btn btn-primary btn-floating mx-1">
            <i class="bi bi-google"></i>
            </button>
          </div>

          <div class="divider d-flex align-items-center my-4">
            <p class="text-center fw-bold mx-3 mb-0">Or</p>
          </div>

          <!-- Email input -->
          <div class="form-outline mb-4">
            
          <?php if(count($errors) > 0):?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <i class="bi bi-exclamation-octagon me-1"></i>
            <strong>Errors: </strong>
            <?php foreach($errors as $error):?>
              <br>
              <?=$error?>
            <?php endforeach;?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif;?> 

        <div class="form-outline mb-2 " >
                            <label class="form-label" for="firstname">First name</label>
                            <input type="text" id="firstname" name="firstname" class="form-control form-control-sm"
                            placeholder="Enter first name (numbers not accepted)" />            
                        </div>
                        <div class="form-outline mb-2 " >
                            <label class="form-label" for="lastname">Last name</label>
                        <input type="text" id="lastname" name="lastname" class="form-control form-control-sm"
                            placeholder="Enter last name (numbers not accepted)" />            
                        </div>
                        <!--<div class="form-outline mb-2 " >
                            <label class="form-label" for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control form-control-sm"
                            placeholder="Enter username (will be used to login)" />            
                        </div>-->

                        <!-- Password input -->
                        <div class="form-outline mb-2 " >
                            <label class="form-label" for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control form-control-sm"
                            placeholder="Enter a valid email" />
                        </div>
                        <div class="form-outline mb-2 " >
                            <label class="form-label" for="phone">Phone</label>
                        <input type="phone" id="phone" name="phone" class="form-control form-control-sm"
                            />
                        </div>
                        </div>
                        <div class="text-center text-lg-start mt-4 pt-2">
                            <input type="submit" class="btn btn-primary btn-lg"
                            style="padding-left: 2.5rem; padding-right: 2.5rem;" id="register" name="register" value="Register" focus>
                            <p class="small fw-bold mt-2 pt-1 mb-0">Already have an account <a href="<?=ROOT?>"
                                class="link-danger">Login</a></p>
                        </div>
                    </div>


        </form>
      </div>
    </div>
  </div>
  <div
    class="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-4 px-4 px-xl-5 bg-primary">
    <!-- Copyright -->
    <div class="text-white mb-3 mb-md-0">
      Copyright © <?=Date("Y");?>. All rights reserved.

      <!-- <div class="text-white text-center mb-3 mb-md-0">
              BUDGET APP
    </div> -->
    </div>
    <!-- Copyright -->

    <!-- Right -->
    <div>
      <a href="#!" class="text-white me-4">
          
      </a>
      <a href="#!" class="text-white me-4">
        
      </a>
      <a href="#!" class="text-white me-4">
        
      </a>
      <a href="" class="text-white" style="font-size: 24pt; text-decoration:none; font-weight: bolder">
          BUDGET APP
      </a>
    </div>
    <!-- Right -->
  </div>
</section>

<?php $this->view('includes/footer')?>

<script>
  // Loop over them and prevent submission
Array.from(forms).forEach(form => {
 form.addEventListener('register', event => {
    
     if (!form.checkValidity()) {    

        
         event.preventDefault()
         event.stopPropagation()
     }

     form.classList.add('was-validated')
 }, false)
})()

</script>