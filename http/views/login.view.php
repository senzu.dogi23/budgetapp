
<?php $this->view('includes/header')?>

<section class="vh-100">
  <div class="container-fluid h-custom">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-9 col-lg-6 col-xl-6">
      <!--https://macaubusiness.s3.ap-southeast-1.amazonaws.com/2021/05/budget.jpeg-->
        <img src="<?=ASSETS?>/images/img1.jpg"
          class="img-fluid" alt="Sample image" >
      </div>

      <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
        <form method="POST" class="row g-3 needs-validation" nonvalidate>
        
          <div class="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
            <p class="lead fw-normal mb-0 me-3">Sign in with</p>
            <button type="button" class="btn btn-primary btn-floating mx-1">
              <i class="bi bi-facebook"></i>
            </button>

            <button type="button" class="btn btn-primary btn-floating mx-1">
              <i class="bi bi-google"></i>
            </button>
            <!-- <button type="button" class="btn btn-primary btn-floating mx-1">
            <div class="g-signin2" data-onsuccess="onSignIn"></div>             
            </button>-->
          </div>

          <div class="divider d-flex align-items-center my-4">
            <p class="text-center fw-bold mx-3 mb-0">Or</p>
          </div>

          <!-- Email input -->
          <div class="form-outline mb-4">
            
          <?php if(count($errors) > 0):?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <i class="bi bi-exclamation-octagon me-1"></i>
            <strong>Errors: </strong>
            <?php foreach($errors as $error):?>
              <br>
              <?=$error?>
            <?php endforeach;?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif;?> 

          <label class="form-label" for="username">Username</label>
            <input type="username" id="username" name="username" class="form-control form-control-lg"
              placeholder="Enter a username" />            
          </div>

          <!-- Password input -->
          <div class="form-outline mb-3">
            <label class="form-label" for="password">Password</label>
            <input type="password" id="password" name="password" class="form-control form-control-lg"
              placeholder="Enter password" />
            
          </div>

          <div class="d-flex justify-content-between align-items-center">
            <!-- Checkbox -->
            <div class="form-check mb-0">
              <input class="form-check-input me-2" type="checkbox" value="" id="rememberme" name="rememberme" />
              <label class="form-check-label" for="rememberme">
                Remember me
              </label>
            </div>
            <a href="<?=ROOT?>/users" class="text-body">Forgot password?</a>
          </div>

          <div class="text-center text-lg-start mt-4 pt-2">
            <input type="submit" class="btn btn-primary btn-lg"
              style="padding-left: 2.5rem; padding-right: 2.5rem;" value="login" name="login">
            <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <a href="<?=ROOT?>/users"
                class="link-danger">Register</a></p>
          </div>

        </form>
      </div>
    </div>
  </div>
  <div
    class="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-4 px-4 px-xl-5 bg-primary">
    <!-- Copyright -->
    <div class="text-white mb-3 mb-md-0">
      Copyright © <?=Date("Y");?>. All rights reserved.

      <!-- <div class="text-white text-center mb-3 mb-md-0">
              BUDGET APP
    </div> -->
    </div>
    <!-- Copyright -->

    <!-- Right -->
    <div>
      <a href="#!" class="text-white me-4">
          
      </a>
      <a href="#!" class="text-white me-4">
        
      </a>
      <a href="#!" class="text-white me-4">
        
      </a>
      <a href="" class="text-white" style="font-size: 24pt; text-decoration:none; font-weight: bolder">
          BUDGET APP
      </a>
    </div>
    <!-- Right -->
  </div>
</section>

<?php $this->view('includes/footer')?>