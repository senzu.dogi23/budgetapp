<style>
    input {
        border-width: 0; 
        box-shadow: none;
    }
    input:focus {
        border-width: 0; 
        box-shadow: none;
    }
</style>

<?php $this->view('includes/header')?>
<?php $this->view('includes/sidebar')?>



            <h2 class="text-center">MONTHLY BUDGET</h2>
                
            <div class="card-group ">
            <div style=""><h3>Budget Dated: <?php//get_day()?>  <?=date('F, Y')?> </h3></div>
                    <form action="" method="GET" class="">
                        <div class="input-group float-end" style="max-width: 250px;margin-left:100px">
                            <div class="input-group-prepend " >
                                <button class="input-group-text btn-sm btn-primary" id="basic-addon1"><i class="bi bi-search btn-sm btn-primary"></i></button>
                            </div>
                            <select id="budgettype" value="<?=isset($_GET['find']) ? $_GET['find']:''?>" name="find" class="form-control form-control-sm"></span>
                                <option <?=get_select('budgettype', 'Income')?> value="2">All</option>    
                                <option <?=get_select('budgettype', 'Income')?> value="1">Income</option>
                                <option <?=get_select('budgettype', 'Expenditure')?> value="0">Expenditure</option>
                                                                    
                            </select>
                        </div>
                    </form>
                    <table class="table table-striped table-hover table-bordered">                        

                    <tr class="" ><th style="width:2px;">#</th><th>Budget Type</th><th>Sub Type</th><th style="width:100px;">Amount</th><th>Budget Date</th><th>Description</th><th>Action</th>
                    </tr>

                    <?php if($rows):?>
                    <?php $count = 1; foreach ($rows as $row): ?>
                        <tr class="">
                            <td class="bg-white"><?=$count?></td>
                            <td class="bg-white">
                                <?php if ($row->budgettype == 1):?>
                                    <?php echo "Income"; ?>
                                <?php else: ?>
                                    <?php echo "Expenditure"; ?>
                                <?php endif ?>
                                
                            </td>
                            <td class="bg-white" ><?=$row->subtype?></td>
                            <td class="bg-white"><?=$row->amount?></td>
                            <td class="bg-white"><?=$row->budgetdate?></td>
                            <td class="bg-white"><?=$row->description?></td>
                            <td><a onclick="deletes(event)" href="<?=ROOT?>/dashboard/delete/<?=$row->id?>" class="btn btn-sm btn-danger" style="backgroud-color:red" ><i class="bi bi-trash"></i></a></td>
                            
                        </tr>                    
                        
                    <?php $count+=1; endforeach ?>
                    <?php endif?>

                    <form method="POST" class="row g-3 needs-validation form-inline" nonvalidate>
                    <tr>
                        <td style="color:red">:-></td>
                        <td>
                            <div class="form-outline mb-2" >
                                <select id="budgettype" name="budgettype" class="form-control form-control-sm">
                                    <option value="1">Income</option>
                                    <option value="0">Expenditure</option>
                                    
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="form-outline mb-2" >
                                <input type="text" id="subtype" name="subtype" class="form-control form-control-sm"
                                placeholder="Eg. Food stuff, light bill etc" autofocus required/>            
                            </div>
                        </td>
                        <td>
                            <div class="form-outline mb-2" >
                                <input type="number" id="amount" name="amount" class="form-control form-control-sm"
                                placeholder="Enter amount" required/>
                            </div>
                        </td>
                        <td>
                            <div class="form-outline mb-2 " >
                                <input type="date" id="budgetdate" name="budgetdate" class="form-control form-control-sm"
                                require/>
                            </div>
                        </td>
                        <td>
                            <div class="form-outline mb-2">
                                <input type="text" id="description" name="description" class="form-control form-control-sm"
                                    placeholder="not more than 100 characters" required/>            
                            </div>
                        </td>
                        <td>
                            <button type="submit" value="" class="form-control form-control-sm btn-sm btn-success"><i class="bi bi-plus"></i></button>
                    
                        </td>
                    </tr>
                    
                    </form>                    
                    
                    </table>
                    <h5>Total Income(Ghc): <?=$budget['income'];?> </h5> <h5 style="margin-left: 100px">Total Expenditure(Ghc): <?=$budget['expenditure'];?> </h5> <h5 style="margin-left: 100px">Balance(Ghc): <?=$budget['balance'];?>  </h5>
                    
                    <div class="row justif-content-right" style="margin-left: 150px"><a href="<?=ROOT?>/dashboard/detailed"><i>View more</i></a></div>
                    
                     
        <!-- /#page-content-wrapper -->

    <?php $this->view('includes/footer')?>

<script type="text/javascript">
    function deletes(e)
    {
        a=confirm('Are You Sure To Remove This Record ?')
        if(a)
        {
            window.location.href='<?=ROOT?>/dashboard/delete/'+id;
        }else{
            e.preventDefault();
        }
    }
</script>
   