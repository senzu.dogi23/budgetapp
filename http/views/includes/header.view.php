
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="keywords">

  <title>Budget App</title>
  

  

  <!-- Vendor CSS Files -->
<!--   <link href="<?=ASSETS?>/css/bootstrap.min.css" rel="stylesheet">
 -->  <link href="<?=ASSETS?>/css/all.min.css" rel="stylesheet">
  <link href="<?=ASSETS?>/styles/style.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.7/css/tether.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet">
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?=ASSETS?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=ASSETS?>/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=ASSETS?>/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=ASSETS?>/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="<?=ASSETS?>/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?=ASSETS?>/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?=ASSETS?>/vendor/simple-datatables/style.css" rel="stylesheet">

  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

  

<!--   <link rel="stylesheet" href="<?=ASSETS?>/styles/tailwindstyles.css">
 -->
  <script src="<?=ASSETS?>/js/jquery.min.js"></script>

 
</head>

<body>
  <main id="main" class="main">
    <div id="wrapper">
        <div id="page-content-wrapper">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-lg-offset-2">
          