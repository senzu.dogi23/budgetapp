<style>
    .logout{
      position: absolute;
      bottom: 70px;
    }
    .logout-divider{
      position: absolute;
      bottom: 100px;
    }
</style>

<div class="overlay">
</div>
    
        <!-- Sidebar -->
    <nav class="navbar navbar-inverse fixed-top" id="sidebar-wrapper" role="navigation">
    <button type="button" class="hamburger animated fadeInLeft is-closed toggler" data-toggle="offcanvas">
      <span class="hamb-top"></span>
      <span class="hamb-middle"></span>
      <span class="hamb-bottom"></span>
    </button>

     <ul class="nav sidebar-nav">
       <div class="sidebar-header">
       <div class="sidebar-brand">
         <a href="#"><h2><?=Auth::get_user()?></h2> </a></div></div>
        <li><a href="<?=ROOT?>/dashboard">Budgets</a></li>
       <li><a href="<?=ROOT?>/users/statistics">Statistics</a></li>
       <li class="dropdown">
          <a href="#" class="dropdown-toggle"  data-toggle="dropdown">Settings <span class="caret"></span></a>
          <ul class="dropdown-menu animated fadeInLeft" >
            <li><a href="<?=ROOT?>/users/resetsystem">Reset System</a></li>
            <li><a href="<?=ROOT?>/users/resetpassword">Change Password</a></li>
            <li><a href="<?=ROOT?>/users/export">Export Data</a></li>
          </ul>
      </li>
            
      </ul>
      
      <div class="divider d-flex align-items-center my-4">
      </div>

      <a style="border-top: 2px solid white ; width:300px; color:white;" class="logout-divider"></a>
      <a href="<?=ROOT?>/logout" style="text-decoration:none; color: white; margin-left:80px" class="logout"><h6>Logout</h6></a>
  </nav>