
<?php
/**
 * User Model
 */

class User extends Model
{
    protected $table = "users";

    protected $allowedColumns = [
        'firstname',
        'lastname',
        'phone',
        'email',
        'username',
        'userid'
    ];

    protected $beforeInsert = [
        'hash_password',
        'make_user_id',

    ];

    protected $beforeUpdate = [
        'hash_password',
    ];

    protected $beforeLogin = [
        'unhash_password'
    ];

    public function validate($DATA, $id = '')
    {
        $this->errors = array();        
        
        if (isset($_POST['register'])) {           
        
                //check for firstname is having letters or empty
                if(!preg_match('/^[a-zA-Z]+$/',$DATA['firstname']) && !empty($DATA['firstname']) )
                {
                    $this->errors['firstname']="Only letters allowed for firstname";
                }
                elseif (empty($DATA['firstname']))
                {
                    $this->errors['firstname']="Firstname can't be empty";
                }

                 //check for lastname is having letters or empty
                 if(!preg_match('/^[a-zA-Z]+$/',$DATA['lastname']) && !empty($DATA['lastname']))
                 {
                     $this->errors['lastname']="Only letters allowed for lastname";
                 }
                 elseif (empty($DATA['lastname']))
                 {
                     $this->errors['lastname']="Lastname can't be empty";
                 }

                //check for phonenumber is having number or empty
                if(!preg_match('/^[0-9]+$/',$DATA['phone']) && !empty($DATA['phone']))
                {
                    $this->errors['phone']="Only numbers allowed in phone number";
                }
                elseif (empty($DATA['phone']))
                {
                    $this->errors['phone']="phone number can't be empty";
                }
                /*elseif (!count($DATA['phonenumber']) < 10 || !count($DATA['phonenumber']) > 15)
                {
                    $this->errors['phonenumber']="Phone number can't be < 10 or > 15";
                }*/
          
                //check for email exists
                if(trim($id) == ""){
                    if($this->first('username', $DATA['email'])) 
                    {
                        $this->errors['email']="Email already exists";
                    }
                }
                

                //check for email is having letters or empty
                if(!filter_var($DATA['email'], FILTER_VALIDATE_EMAIL)) 
                {
                    $this->errors['email']="Not a valid email";
                }
        }
        
        if(count($this->errors) <= 0)
        {
            return true;
        }
        return false;
    }
    
    public function hash_password($data)
    {
        $data = password_hash($data, PASSWORD_DEFAULT);

        return $data;
    }
    
}