
<?php

/**
 * Authentication c
 */
class Auth
{
    public static function authenticate($row)
    {
        
        $_SESSION['USER'] = $row;
        
        //show($_SESSION['USER']);die();

    }
    
    public static function logout()
    {
        if(isset($_SESSION['USER']))
        {
            unset($_SESSION['USER']);
        }
    }

    public static function logged_in()
    {
        if(isset($_SESSION['USER']))
        {
           return true;
        }
        return false;
    }

    public static function get_user(){
        if(isset($_SESSION['USER']))
        {
            
            return $_SESSION['USER']->lastname .' '. $_SESSION['USER']->firstname;
        }
        return false;
    }

    public static function get_id(){
        if(isset($_SESSION['USER']))
        {
            return $_SESSION['USER']->userid;
        }
        return false;
    }



    public static function get_role()
    {
        if(isset($_SESSION['USER']))
        {
            return $_SESSION['USER']->role;
        }
        return false;
    }

    public static function get_image()
    {
        if(isset($_SESSION['USER']))
        {
            return $_SESSION['USER']->image;
        }
        return false;
    }

    public static function __callStatic($method, $params)
    {
        $prop = strtolower(str_replace("get", "",$method));
        if(isset($_SESSION['USER']->$prop))
        {
           return $_SESSION['USER']->$prop;
        }
        return 'Unknown';
    }

    public static function switch_church($id)
    {
        if(isset($_SESSION['USER']) && $_SESSION['USER']->user_rank == "super_admin")
        {
            $user = new User();
            $church = new Church();

            if($row = $church->where('id', $id))
            {
                $row = $row[0];
                $arr['church_id'] = $row->church_id;

               if($user->update($_SESSION['USER']->id,$arr))
               {
                    $_SESSION['USER']->church_id = $row->church_id;
                    $_SESSION['USER']->church_name = $row->church;
               }
            }
            return true; 
        }
        return false;
    }


    public static function unhash_password($data)
    {
        $sSalt = '20adeb83e85f03cfc84d0fb7e5f4d290';
        $sSalt = substr(hash('sha256', $sSalt, true), 0, 32);
        $method = 'aes-256-cbc';
    
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
    
        $data = openssl_decrypt(base64_decode($data), $method, $sSalt, OPENSSL_RAW_DATA, $iv);
        #$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        return $data;
    }

    public static function access($rank = 'Member')
    {
        if(!isset($_SESSION['USER']))
        {
           return false;
        }

        $logged_in_rank = $_SESSION['USER']->role;

        $RANK['super_admin']    = ['super_admin','Media','Secretary','Minister','Administrator','Member', 'Leader'];
        $RANK['Administrator']  = ['Media','Finance','Secretary','Minister','Administrator','Member', 'Leader'];
        $RANK['Minister']       = ['Media','Secretary','Member', 'Leader'];
        $RANK['Secretary']      = ['Media','Secretary','Member'];
        $RANK['Finance']        = ['Finance','Media','Member', 'Leader'];
        $RANK['Leader']         = ['Media','Member', 'Leader'];
        $RANK['Media']          = ['Media','Member'];
        $RANK['Member']         = ['Member'];

        if(!isset($RANK[$logged_in_rank]))
        {
            return false;
        }
        if(in_array($rank,$RANK[$logged_in_rank],))
        {
            return true;
        }

        return false;
    }

    public static function i_own_content($row)
    {
        if(!isset($_SESSION['USER']))
        {
           return false;
        }

        if(isset($row->user_id))
        {
            if($_SESSION['USER']->user_id == $row->user_id){
                return true;
            }
        }

        $allowed[] = 'super_admin';
        $allowed[] = 'Administrator';
        if(in_array($_SESSION['USER']->user_rank, $allowed))
        {
            return true;
        }

        return false;
    }

}