
<?php
/**
 * User Model
 */

class User extends Model
{
    protected $table = "user";

    public function validate($DATA, $id = '')
    {
        $this->errors = array();
        //check if username is entered
        if (empty($DATA['username']))
        {
            $this->errors['username']="Enter username";
        }

        //check check if password is entered
        if (empty($DATA['password']))
        {
            $this->errors['password']="Enter password";
        }

        if(count($this->errors) <= 0)
        {
            return true;
        }
        return false;
    }
    
}