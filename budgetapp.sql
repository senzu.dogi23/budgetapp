-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 11, 2023 at 04:35 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `budgetapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `budgets`
--

CREATE TABLE `budgets` (
  `id` int(11) NOT NULL,
  `budgettype` varchar(1) DEFAULT '1',
  `subtype` varchar(50) NOT NULL,
  `amount` decimal(6,2) NOT NULL,
  `budgetdate` date NOT NULL,
  `description` varchar(100) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `month` int(2) NOT NULL,
  `year` varchar(4) NOT NULL,
  `userid` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `budgets`
--

INSERT INTO `budgets` (`id`, `budgettype`, `subtype`, `amount`, `budgetdate`, `description`, `dateadded`, `month`, `year`, `userid`) VALUES
(1, '0', 'Food staff', '50.00', '2022-12-20', 'Buying of food stuff', '2023-01-09 16:09:24', 12, '2022', 'usr00001'),
(2, '0', 'Water', '18.00', '2022-12-20', 'Two sachets of pure water', '2023-01-09 16:09:36', 12, '2022', 'usr00001'),
(4, '1', 'lhjhkjhkjh', '50.00', '2022-12-22', 'sdfdfsdfsdfsdf', '2023-01-09 16:09:42', 12, '2022', 'usr00001'),
(8, '1', 'Food stuff', '50.00', '2022-12-21', 'Buying of food items for the week', '2023-01-09 16:09:46', 12, '2022', 'usr00002'),
(9, '1', 'Food stuff', '50.00', '2022-12-21', 'Buying of food items for the week', '2023-01-09 16:09:50', 12, '2022', 'usr00002'),
(10, '0', 'Light bill', '105.00', '2022-12-21', 'November light bill', '2023-01-09 16:09:56', 12, '2022', 'usr00002'),
(12, '1', 'Salary', '3500.00', '2022-12-21', 'Payment for november salary', '2023-01-09 16:10:00', 12, '2022', 'usr00002'),
(15, '1', 'Salary', '2580.00', '2022-12-21', 'Payment for november salary', '2023-01-09 16:10:06', 12, '2022', 'usr00001'),
(16, '1', 'Salary', '3850.00', '2022-12-29', 'Payment for november salary', '2023-01-09 16:10:11', 12, '2022', 'usr00002'),
(17, '0', 'Water', '120.00', '2022-12-29', 'November light bill', '2023-01-09 16:10:14', 12, '2022', 'usr00002'),
(18, '1', 'Sales', '250.00', '2022-12-29', 'Sales of my pair of shoes', '2023-01-09 16:10:17', 12, '2022', 'usr00002'),
(19, '1', 'Sales', '250.00', '2022-12-29', 'Sales of my pair of shoes', '2023-01-09 16:10:22', 12, '2022', 'usr00002'),
(20, '0', 'Television', '4200.00', '2022-12-29', 'Purchase of 42\'\' flat screen TV set', '2023-01-09 16:10:26', 12, '2022', 'usr00002'),
(26, '0', 'Light bill', '250.00', '2023-01-04', 'Payment for december light bill', '2023-01-09 16:10:35', 1, '2023', 'usr00002'),
(27, '0', 'Water bill', '50.00', '2023-01-03', 'Payment for december water bill', '2023-01-09 16:10:39', 1, '2023', 'usr00002'),
(29, '1', 'Salary', '3500.00', '2023-01-02', 'Payment for december salary', '2023-01-09 16:10:43', 1, '2023', 'usr00002'),
(30, '1', 'Sales', '1200.00', '2023-01-04', 'Sales of TV set', '2023-01-09 16:10:50', 1, '2023', 'usr00002'),
(31, '1', 'Sales', '1200.00', '2023-01-04', 'Sales of TV set', '2023-01-09 16:10:58', 1, '2023', 'usr00002'),
(32, '0', 'Food stuff', '580.00', '2023-01-05', 'Food stuffs for 2 weeks', '2023-01-09 16:11:02', 1, '2023', 'usr00002'),
(33, '1', 'Sales', '150.00', '2023-01-10', 'Sales of old mobile phone', '2023-01-10 14:21:23', 1, '2023', 'usr00002'),
(34, '0', 'Food stuff', '50.00', '2023-01-10', 'Food stuffs for 2 weeks', '2023-01-10 14:22:15', 1, '2023', 'usr00002');

-- --------------------------------------------------------

--
-- Table structure for table `net`
--

CREATE TABLE `net` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `id` int(11) NOT NULL,
  `userid` varchar(10) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dateregistered` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`id`, `userid`, `lastname`, `firstname`, `phone`, `email`, `dateregistered`) VALUES
(1, 'usr00001', 'Blay', 'John', '01234567890', 'blay@gmail.com', '2022-12-20 23:16:58'),
(5, 'usr00002', 'Adobah', 'Eric', '1223555', 'adobah12@gmail.com', '2022-12-20 23:17:20'),
(6, 'usr00003', 'Smith', 'John', '1223555', 'smith@gmail.com', '2022-12-26 16:00:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL,
  `rememberme` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `username`, `password`, `rememberme`) VALUES
(2, 'usr00002', 'adobah12@gmail.com', 'password', b'0'),
(3, 'usr00001', 'blay@gmail.com', 'password', b'0'),
(4, 'usr00003', 'smith@gmail.com', 'password', b'0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `budgets`
--
ALTER TABLE `budgets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `lastname` (`lastname`),
  ADD KEY `firstname` (`firstname`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `budgets`
--
ALTER TABLE `budgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_userdetails` FOREIGN KEY (`userid`) REFERENCES `userdetails` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;